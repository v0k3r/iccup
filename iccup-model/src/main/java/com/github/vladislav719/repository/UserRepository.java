package com.github.vladislav719.repository;

import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by vbaimurzin on 25.08.2015.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
