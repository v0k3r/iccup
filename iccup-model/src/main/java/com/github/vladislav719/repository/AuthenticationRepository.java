package com.github.vladislav719.repository;

import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by vbaimurzin on 25.08.2015.
 */
@Repository
public interface AuthenticationRepository extends JpaRepository<Authentication, Long> {

    @Query("select auth from Authentication auth where auth.user = ?1")
    public Authentication getUserAuthentication(User user);

    @Query("select auth from Authentication auth where auth.loginId = ?1")
    Authentication getAuthenticationByLoginId(String loginId);

    @Query("select auth from Authentication auth where auth.user.name = ?1 or auth.user.email = ?1")
    Authentication getAuthenticationByUsernameOrEmail(String username);
}
