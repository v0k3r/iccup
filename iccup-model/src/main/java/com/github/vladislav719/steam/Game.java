package com.github.vladislav719.steam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vlad on 24.08.15.
 */
public class Game {

    private int appid;
    private String name;
    private String playtime_2weeks;
    private String playtime_forever;
    private String img_icon_url;
    private String img_logo_url;
    private List<Boolean> platforms;
    private List<String> categories;
    private int instances;


    public Game() {
        instances = 0;
        playtime_2weeks = "-1";
        name = "";
        platforms = new ArrayList<Boolean>();
        categories = new ArrayList<String>();
    }

    public int getAppid() {
        return appid;
    }

    public void setAppid(int appid) {
        this.appid = appid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaytime_2weeks() {
        return playtime_2weeks;
    }

    public void setPlaytime_2weeks(String playtime_2weeks) {
        this.playtime_2weeks = playtime_2weeks;
    }

    public String getPlaytime_forever() {
        return playtime_forever;
    }

    public void setPlaytime_forever(String playtime_forever) {
        this.playtime_forever = playtime_forever;
    }

    public String getImg_icon_url() {
        return img_icon_url;
    }

    public void setImg_icon_url(String img_icon_url) {
        this.img_icon_url = img_icon_url;
    }

    public String getImg_logo_url() {
        return img_logo_url;
    }

    public void setImg_logo_url(String img_logo_url) {
        this.img_logo_url = img_logo_url;
    }

    public List<Boolean> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<Boolean> platforms) {
        this.platforms = platforms;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public int getInstances() {
        return instances;
    }

    public void setInstances(int instances) {
        this.instances = instances;
    }
}
