package com.github.vladislav719.steam;

/**
 * Created by vlad on 24.08.15.
 */
public class SteamConfig {

    public static final String HOST = "http://api.steampowered.com";
    public static final String PLAYER_SUMMARIES = "/ISteamUser/GetPlayerSummaries/v0002/";
    public static final String KEY = "?key=3E152C38309F8AFEC6D4A1BA975FEEA8";
    
}
