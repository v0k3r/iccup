package com.github.vladislav719.steam;

import java.util.List;

/**
 * Created by vlad on 24.08.15.
 */
public class SteamAccount {
    private String steamId;
    private List<Game> games;
    private List<SteamAccount> friends;
    private String avatar;
    private String personaName;
    private String realName;
    private String profileURL;
    private List<Game> gamesToPlay;
    private List<Game> gamesToBuy;

    public String getSteamId() {
        return steamId;
    }

    public void setSteamId(String steamId) {
        this.steamId = steamId;
    }

    public List getGames() {
        return games;
    }

    public void setGames(List games) {
        this.games = games;
    }

    public List<SteamAccount> getFriends() {
        return friends;
    }

    public void setFriends(List<SteamAccount> friends) {
        this.friends = friends;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPersonaName() {
        return personaName;
    }

    public void setPersonaName(String personaName) {
        this.personaName = personaName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getProfileURL() {
        return profileURL;
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }

    public List<Game> getGamesToPlay() {
        return gamesToPlay;
    }

    public void setGamesToPlay(List<Game> gamesToPlay) {
        this.gamesToPlay = gamesToPlay;
    }

    public List<Game> getGamesToBuy() {
        return gamesToBuy;
    }

    public void setGamesToBuy(List<Game> gamesToBuy) {
        this.gamesToBuy = gamesToBuy;
    }
}
