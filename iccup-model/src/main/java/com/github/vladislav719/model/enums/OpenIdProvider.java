package com.github.vladislav719.model.enums;

/**
 * Created by vlad on 24.08.15.
 */
public enum OpenIdProvider {
    STEAM,
    VK
}
