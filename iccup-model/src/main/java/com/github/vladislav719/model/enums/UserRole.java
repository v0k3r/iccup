package com.github.vladislav719.model.enums;

/**
 * Created by vbaimurzin on 26.08.2015.
 */
public enum  UserRole {
    USER,
    USER_CONFIRMED,
    USER_FULL
}
