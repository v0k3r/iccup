package com.github.vladislav719.model.enums;

/**
 * Created by vlad on 24.08.15.
 */
public enum  TourneyType {
    X1,
    X2,
    X3,
    X5,
    CHALLENGE1,
    CHALLENGE5
}
