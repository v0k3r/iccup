package com.github.vladislav719.dto;

import java.io.Serializable;

/**
 * Created by vlad on 25.08.15.
 */
public class UserRegistrationForm implements Serializable {
    private String username;
    private String password;
    private String confirmPassword;
    private String email;

    public UserRegistrationForm() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
