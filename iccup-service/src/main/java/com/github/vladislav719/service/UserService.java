package com.github.vladislav719.service;

import com.github.vladislav719.dto.UserRegistrationForm;
import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.model.User;
import org.springframework.stereotype.Service;

/**
 * Created by vlad on 24.08.15.
 */
public interface UserService {

    void createUser(User user);
    Authentication loadUserByUsername(String login);
    User findUserBySteamId(String steamid);
    User createUser(UserRegistrationForm form);
}
