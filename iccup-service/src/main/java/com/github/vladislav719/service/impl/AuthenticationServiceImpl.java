package com.github.vladislav719.service.impl;

import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.model.User;
import com.github.vladislav719.repository.AuthenticationRepository;
import com.github.vladislav719.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by vbaimurzin on 25.08.2015.
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private AuthenticationRepository authenticationRepository;

    @Override
    public Authentication getUserAuthInfo(User user) {
        return authenticationRepository.getUserAuthentication(user);
    }

    @Override
    public Authentication getAuthenticationBySteamId(String steamId) {
        return authenticationRepository.getAuthenticationByLoginId(steamId);
    }

    @Override
    public Authentication getAuthenticationByUserNameOrEmail(String username) {
        return authenticationRepository.getAuthenticationByUsernameOrEmail(username);
    }

    @Override
    public Authentication saveAuthentication(Authentication authentication) {
        return authenticationRepository.save(authentication);
    }
}
