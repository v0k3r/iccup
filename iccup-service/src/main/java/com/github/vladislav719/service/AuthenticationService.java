package com.github.vladislav719.service;

import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.model.User;

/**
 * Created by vbaimurzin on 25.08.2015.
 */
public interface AuthenticationService {
    Authentication getUserAuthInfo(User user);
    Authentication getAuthenticationBySteamId(String steamId);
    Authentication getAuthenticationByUserNameOrEmail(String username);
    Authentication saveAuthentication(Authentication authentication);
}
