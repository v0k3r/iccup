package com.github.vladislav719.service;

import com.github.vladislav719.steam.SteamAccount;

/**
 * Created by vlad on 25.08.15.
 */
public interface SteamService {

    SteamAccount getPlayerSummary(String steamId);

}
