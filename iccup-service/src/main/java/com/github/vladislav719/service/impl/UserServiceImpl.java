package com.github.vladislav719.service.impl;

import com.github.vladislav719.dto.UserRegistrationForm;
import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.model.User;
import com.github.vladislav719.repository.AuthenticationRepository;
import com.github.vladislav719.repository.UserRepository;
import com.github.vladislav719.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by vlad on 24.08.15.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationRepository authenticationRepository;

    @Override
    public void createUser(User user) {
        userRepository.save(user);
    }

    @Override
    public Authentication loadUserByUsername(String login) {
        return authenticationRepository.getAuthenticationByUsernameOrEmail(login);
    }

    @Override
    public User findUserBySteamId(String steamid) {
        return null;
    }

    @Override
    @Transactional
    public User createUser(UserRegistrationForm form) {
        Authentication authentication = new Authentication();
        authentication.setPassword(form.getPassword());
        User user = new User();
        user.setAuthentication(authentication);
        user.setEmail(form.getEmail());
        user.setName(form.getUsername());
        authentication.setUser(user);
        return userRepository.save(user);
    }

}
