package com.github.vladislav719.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vladislav719.service.SteamService;
import com.github.vladislav719.steam.SteamAccount;
import com.github.vladislav719.steam.SteamConfig;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vlad on 25.08.15.
 */
@Service
public class SteamServiceImpl implements SteamService {


    @Override
    public SteamAccount getPlayerSummary(String steamId) {
        String url = SteamConfig.HOST + SteamConfig.PLAYER_SUMMARIES + SteamConfig.KEY + "&steamids=" + steamId;
        SteamAccount steamAccount = getUser(retrieve(url).get(0));
        return steamAccount;
    }

    private SteamAccount getUser(Map map) {
        SteamAccount steamAccount = new SteamAccount();
        String userId = (String) map.get("steamid");
        steamAccount.setSteamId(userId);

        String personaname = (String) map.get("personaname");
        steamAccount.setPersonaName(personaname);

        String realName = (String) map.get("realname");
        steamAccount.setRealName(realName);

        String imgUrl = (String) map.get("avatarfull");
        steamAccount.setAvatar(imgUrl);

        String profileUrl = (String) map.get("profileurl");
        steamAccount.setProfileURL(profileUrl);

        return steamAccount;
    }

    private List<Map> retrieve(String url) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> JSONMap = null;
        try {
            JSONMap = mapper.readValue(new URL(url), Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map response = (Map) JSONMap.get("response");
        List<Map> playerMap = (List<Map>) response.get("players");
        return playerMap;
    }
}
