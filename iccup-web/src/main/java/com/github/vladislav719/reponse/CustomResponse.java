package com.github.vladislav719.reponse;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by vbaimurzin on 25.08.2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomResponse<T> {
    private final int status;
    private final String message;
    private T response;

    public CustomResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public T getT() {
        return response;
    }

    public void setT(T t) {
        this.response = t;
    }
}
