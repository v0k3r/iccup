package com.github.vladislav719.util.impl;

import com.github.vladislav719.auth.TokenService;
import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.model.User;
import com.github.vladislav719.service.AuthenticationService;
import com.github.vladislav719.service.UserService;
import com.github.vladislav719.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by vbaimurzin on 26.08.2015.
 */
public class SecurityUtilImpl implements SecurityUtil {


    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @Override
    public Authentication getCurrentUserAuthentication(String token) {
        return (Authentication) tokenService.retrieve(token).getPrincipal();
    }



}
