package com.github.vladislav719.util;


import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.model.User;

/**
 * Created by vbaimurzin on 26.08.2015.
 */
public interface SecurityUtil {
    Authentication getCurrentUserAuthentication(String token);
}
