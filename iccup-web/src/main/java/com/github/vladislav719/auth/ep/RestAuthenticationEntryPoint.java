package com.github.vladislav719.auth.ep;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vladislav719.reponse.CustomResponse;
import com.github.vladislav719.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vlad on 25.08.15.
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint, AccessDeniedHandler {

    @Autowired
    private ObjectMapper msgMapper;

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        writeError(httpServletResponse, true);
    }

    private void writeError(HttpServletResponse response, boolean isNoAuth) {
        CustomResponse errorResponse = new CustomResponse(2, "access_denied");
        if (isNoAuth) {
            WebUtil.objToResponse(errorResponse, response, HttpServletResponse.SC_UNAUTHORIZED, msgMapper);
        } else {
            WebUtil.objToResponse(errorResponse, response, HttpServletResponse.SC_METHOD_NOT_ALLOWED, msgMapper);
        }
    }

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        writeError(httpServletResponse, false);
    }
}
