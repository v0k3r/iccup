package com.github.vladislav719.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vbaimurzin on 25.08.2015.
 */
public class TokenResponse {

    @JsonProperty
    private String token;

    public TokenResponse() {
    }

    public TokenResponse(String token) {
        this.token = token;
    }
}
