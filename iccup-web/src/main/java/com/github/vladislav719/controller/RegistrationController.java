package com.github.vladislav719.controller;

import com.github.vladislav719.auth.SteamOpenID;
import com.github.vladislav719.dto.UserRegistrationForm;
import com.github.vladislav719.model.Authentication;
import com.github.vladislav719.reponse.CustomResponse;
import com.github.vladislav719.service.AuthenticationService;
import com.github.vladislav719.service.SteamService;
import com.github.vladislav719.service.UserService;
import com.github.vladislav719.steam.SteamAccount;
import com.github.vladislav719.util.SecurityUtil;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vlad on 25.08.15.
 */
@Controller
public class RegistrationController {


    /**
     * Сначало пользователь просто регистрируется по логину и паролю, затем он может привязать аккаунт к стимИд
     */
    private Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    private SteamService steamService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private SteamOpenID steamOpenID;

    @RequestMapping(value = "/test")
    @ResponseBody
    public Authentication test(@RequestParam String token) {
        return securityUtil.getCurrentUserAuthentication(token);
    }

    @RequestMapping(value = "/api/v1/test", method = RequestMethod.GET)
    public String testSec() {
        return "ok";
    }

    @RequestMapping(value = "/assignId", method = RequestMethod.GET)
    public void assignOpenId(@RequestParam String token, HttpServletResponse response, HttpServletRequest request) throws IOException {
        Authentication authentication = securityUtil.getCurrentUserAuthentication(token);
        if (authentication.getLoginId() != null) {
            response.sendRedirect("/");
        }
        StringBuffer requestURL = request.getRequestURL();
        int lastSlashIndex = requestURL.lastIndexOf("/");
        String callBackURL = requestURL.substring(0, lastSlashIndex) + "/SteamCallBack";

        String steamURL = steamOpenID.login(callBackURL);
        logger.info(steamURL);
        response.setHeader("token", token);
        request.setAttribute("token", token);
        response.sendRedirect(steamURL);
    }

    @RequestMapping(method = RequestMethod.POST, produces = {"application/json"},value = "/registration")
    public @ResponseBody
    Object createUser(@ModelAttribute UserRegistrationForm form, HttpServletResponse response) {
        if (isExist(form)) {
            response.setStatus(HttpServletResponse.SC_CONFLICT);
            return new CustomResponse<>(3, "Username already exist");
        }
        if (!isPasswordEquals(form)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return new CustomResponse<>(4, "Password confirmation and Password must match ");
        }
        if (!isEmailValid(form.getEmail())) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return new CustomResponse<>(5, "Invalid Email");
        } else {
            if (isEmailExist(form)) {
                response.setStatus(HttpServletResponse.SC_CONFLICT);
                return new CustomResponse<>(6, "Email already exist");
            }
        }
        form.setPassword(decodePassword(form.getPassword()));
        response.setStatus(HttpServletResponse.SC_CREATED);
        return userService.createUser(form);
    }

    @RequestMapping(value = "/SteamCallBack")
    @ResponseBody
    public CustomResponse callback(HttpServletRequest request) {
        String steamId = steamOpenID.verify(request.getRequestURL().toString(), request.getParameterMap());
        logger.info("New User with steamId: " + steamId);
        Authentication authentication = authenticationService.getAuthenticationBySteamId(steamId);
        if (authentication != null) {
            //пользователь уже зарегистирован в системе, можно отправлять на главную
            return new CustomResponse(1, "success");
        } else {
            //Отправляем на страницу дальнейшей регистрации и передаем данные его аккаунта полученные по стим апи
//            String token = request.getParameter("token");
//            authentication = securityUtil.getCurrentUserAuthentication(token);
//            authentication.setLoginId(steamId);
//            authenticationService.saveAuthentication(authentication);
            CustomResponse<SteamAccount> response = new CustomResponse<>(2, "steamId assigned");
            response.setT(steamService.getPlayerSummary(steamId));
            return response;
        }
    }

    private String decodePassword(String password) {
        return shaPasswordEncoder.encodePassword(password, "");
    }
    private boolean isEmailExist(UserRegistrationForm form) {
        if (authenticationService.getAuthenticationByUserNameOrEmail(form.getEmail()) != null)
            return true;
        return false;
    }

    private boolean isExist(UserRegistrationForm userRegistrationForm) {
        if (authenticationService.getAuthenticationByUserNameOrEmail(userRegistrationForm.getUsername()) != null)
            return true; //user exist
        return false;
    }

    private boolean isPasswordEquals(UserRegistrationForm userRegistrationForm) {
        if (userRegistrationForm.getPassword().equals(userRegistrationForm.getConfirmPassword()))
            return true; //password are equals
        return false;
    }

    private boolean isEmailValid(String email) {
        return EmailValidator.getInstance().isValid(email);
    }
}
