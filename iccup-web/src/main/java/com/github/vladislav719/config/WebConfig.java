package com.github.vladislav719.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vladislav719.auth.SteamOpenID;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Created by vlad on 24.08.15.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.github.vladislav719.controller"})
public class WebConfig extends WebMvcConfigurerAdapter {


    @Bean
    public ViewResolver resolver() {
        InternalResourceViewResolver url = new InternalResourceViewResolver();
        url.setPrefix("/WEB-INF/");
        url.setSuffix(".html");
        return url;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//        super.configureDefaultServletHandling(configurer);
        configurer.enable();
    }

    @Bean
    public SteamOpenID steamOpenID() {
        return new SteamOpenID();
    }

    @Bean
    public String getBean(ObjectMapper mapper) {
        return "";
    }
}
